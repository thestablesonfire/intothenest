import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';

import {CommandInputComponent} from './command-input/command-input.component';
import {DungeonComponent} from './dungeon/dungeon.component';
import {MapFactory} from './dungeon/Map/map-factory';
import {MessageBoxComponent} from './message-box/message-box.component';
import createSpy = jasmine.createSpy;

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                CommandInputComponent,
                DungeonComponent,
                MessageBoxComponent
            ],
            providers: [
                { provide: MapFactory, useClass: createSpy('MapFactory') }
            ]
        }).compileComponents();
    }));
    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
});
