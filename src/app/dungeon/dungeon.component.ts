import {Component, OnInit} from '@angular/core';
import {Map} from './Map/map';
import {MapFactory} from './Map/map-factory';

@Component({
    selector: 'app-dungeon',
    templateUrl: './dungeon.component.html',
    styleUrls: ['./dungeon.component.scss']
})
export class DungeonComponent implements OnInit {
    /**
     * Creates Maps
     */
    mapFactory: MapFactory;

    /**
     * The Maps for the Dungeon
     */
    maps: Map[];

    /**
     * The index of the current Map
     */
    currentIndex: number;

    /**
     * Displays all the player can see that happens on the map
     */
    constructor(mapFactory: MapFactory) {
        this.maps = [];
        this.mapFactory = mapFactory;
    }

    ngOnInit() {
        this.maps.push(this.mapFactory.createMap(0));
        this.currentIndex = 0;
    }
}
