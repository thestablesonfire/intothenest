import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import { DungeonComponent } from './dungeon.component';
import {MapFactory} from './Map/map-factory';
import createSpyObj = jasmine.createSpyObj;
import createSpy = jasmine.createSpy;

describe('DungeonComponent', () => {
    let component: DungeonComponent;
    let fixture: ComponentFixture<DungeonComponent>;
    let mapSpy = createSpy('Map');

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                DungeonComponent
            ],
            providers: [
                { provide: MapFactory, useValue: createSpyObj('MapFactory', {'createMap': mapSpy}) }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DungeonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create a map and push it to array',() => {
        const mapFactorySpy = TestBed.get(MapFactory);
        expect(mapFactorySpy.createMap).toHaveBeenCalledTimes(1);
        expect(component.maps.length).toBe(1);
        expect(component.maps[0]).toEqual(mapSpy);
    });
});
