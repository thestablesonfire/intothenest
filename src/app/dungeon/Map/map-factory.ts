import {Room} from './Room/room';
import {RoomFactory} from './Room/room-factory';
import {Coordinate} from './Tile/Coordinate/coordinate';
import {Map} from './map';
import {Tile} from './Tile/tile';
import {TileType} from './Tile/tile-type';
import {UtilitiesService} from './Utilities/utilities.service';

export class MapFactory {
    /**
     * The Map currently being built
     */
    map: Map;

    /**
     * Creates Rooms
     */
    roomFactory: RoomFactory;

    /**
     * The minimum number of rooms a map can have
     */
    minRooms: number;

    /**
     * The maximum number of rooms a map can have
     */
    maxRooms: number;

    /**
     * The size of each map
     */
    dimension: number;

    /**
     * Common utilities
     */
    utilitiesService: UtilitiesService;

    /**
     * Creates Maps
     */
    constructor() {
        this.roomFactory = new RoomFactory();
        this.minRooms = 6;
        this.maxRooms = 9;
        this.dimension = 40;
        this.utilitiesService = new UtilitiesService();
    }

    /**
     * Crates a new Map
     * @param z
     * @return {Map}
     */
    createMap(z) {
        this.map = new Map(z);
        this.map.dimension = this.dimension;
        this.generateBlankMap();
        this.createRooms();
        this.createHallways();
        this.fillBlankPerimeters();

        return this.map;
    }

    /**
     * Fills the Map in with BLANK tiles
     */
    generateBlankMap() {
        let row;

        for (let y = 0; y < this.dimension; y++) {
            row = [];
            for (let x = 0; x < this.dimension; x++) {
                row.push(new Tile(
                    new Coordinate(x, y, this.map.zLevel),
                    TileType.BLANK
                ));
            }
            this.map.tiles.push(row);
        }
    }

    /**
     * Generates all the rooms for the Map
     */
    createRooms() {
        const numRooms = UtilitiesService.generateRandomNumber(this.minRooms, this.maxRooms);
        let totalCount = 0;

        let location;
        for (let i = 0; i < numRooms && totalCount < 500; i++) {
            location = UtilitiesService.generateRandomCoordinate(this.dimension, this.dimension, this.map.zLevel);
            const room = this.roomFactory.createRoom(location);

            if (this.roomFitsOnMap(room) && this.roomDoesNotOverlap(room)) {
                this.addRoom(room);
            } else {
                i--;
            }

            totalCount++;
        }
    }

    /**
     * Returns true if the passed room fits on the map
     * @param {Room} room
     * @return {boolean}
     */
    roomFitsOnMap(room: Room) {
        return room.location.x + room.width < this.dimension - 1 &&
            room.location.y + room.height < this.dimension - 1;
    }

    /**
     *
     * @param {Room} room
     */
    roomDoesNotOverlap(room: Room) {
        for (let y = 0; y < room.height; y++) {
            for (let x = 0; x < room.width; x++) {
                if (this.map.tiles[room.location.y + y][room.location.x + x].type !== TileType.BLANK) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Adds a Room to a Map
     * @param {Room} room
     */
    addRoom(room: Room) {
        this.map.rooms.push(room);
        this.addTiles(room);
    }

    /**
     * Adds the room's tiles to the Map
     * @param {Room} room
     */
    addTiles(room: Room) {
        for (let y = 0; y < room.height; y++) {
            for (let x = 0; x < room.width; x++) {
                this.map.tiles[room.location.y + y][room.location.x + x] = room.tiles[y][x];
            }
        }
    }

    /**
     * Creates all the hallways for the map
     */
    createHallways() {
        const paths = [];
        let start, end;
        let otherRooms;
        let otherDoors;

        for (let room of this.map.rooms) {
            otherRooms = this.getOtherRooms(room);
            otherDoors = this.getAllDoors(otherRooms);

            for (let door of room.doors) {
                let randomDoor = otherDoors[UtilitiesService.generateRandomNumber(0, otherDoors.length - 1)];
                let startLocation = door.location;
                let endLocation = randomDoor.location;
                start = this.map.tiles[startLocation.y][startLocation.x];
                end = this.map.tiles[endLocation.y][endLocation.x];
                paths.push(this.utilitiesService.findPath(start, end, this.map));
                otherDoors.splice(otherDoors.indexOf(randomDoor), 1);
            }
        }

        for (let path of paths) {
            this.createHallway(path);
        }
    }

    /**
     * Returns all rooms on the map that are not the room passed in
     * @param {Room} room
     * @return {Room[]}
     */
    getOtherRooms(room: Room) {
        return this.map.rooms.filter(mapRoom => {
            return mapRoom !== room;
        });
    }

    /**
     * Returns all the doors from the list of rooms passed
     * @param {Room[]} rooms
     * @return {any[]}
     */
    getAllDoors(rooms: Room[]) {
        let otherDoors = [];

        for (let room of rooms) {
            otherDoors = otherDoors.concat(room.doors);
        }

        return otherDoors;
    }

    /**
     * Builds a hallway from one point to another
     * @param {Tile[]} path
     */
    createHallway(path: Tile[]) {
        let adjacentTiles;
        for (let tile of path) {
            tile.type = TileType.FLOOR;
            adjacentTiles = this.map.getAdjacentTiles(tile, true);
            for (let adjacentTile of adjacentTiles) {
                if (adjacentTile.type === TileType.BLANK) {
                    adjacentTile.type = TileType.WALL;
                }
            }
        }
    }

    /**
     * Gets all the BLANK Tiles next to perimeter Tiles and makes them WALLs
     */
    fillBlankPerimeters() {
        let perimeterTiles = [];

        for (let room of this.map.rooms) {
            for (let tile of room.perimeter) {
                perimeterTiles = perimeterTiles.concat(
                    this.map.getAdjacentTiles(tile, true).filter(tile => {
                        return tile.type === TileType.BLANK;
                    })
                );
            }
        }

        for (let tile of perimeterTiles) {
            tile.type = TileType.WALL;
        }
    }
}
