import {Map} from '../map';
import {Tile} from '../Tile/tile';
import {TileType} from '../Tile/tile-type';
import {VisitMap} from './visit-map';

export class Pathfinder {
    /**
     * The start Tile for the path
     */
    start: Tile;

    /**
     * The end Tile for the path
     */
    end: Tile;

    /**
     * The map to find the path on
     */
    map: Map;

    /**
     * The queue of tiles to search
     */
    queue: Tile[];

    /**
     * The map that keeps track of which tiles we've already searched
     */
    visitMap: VisitMap;

    /**
     * The array of tiles in the found path
     */
    path: Tile[];

    /**
     * Finds a path from start to end
     */
    constructor() {
        this.queue = [];
    }

    /**
     * Returns an array of tiles from start to end if it exists
     * @param {Tile} start
     * @param {Tile} end
     * @param {Map} map
     * @return {Tile[]}
     */
    findPath(start: Tile, end: Tile, map: Map) {
        this.start = start;
        this.end = end;
        this.map = map;
        this.visitMap = new VisitMap(this.map.dimension);

        this.breadthFirstSearch();
        const path = this.path;
        this.reset();

        return path;
    }

    breadthFirstSearch() {
        let currentTile;
        this.queue.push(this.start);
        this.visitMap.markMap(this.start.location.x, this.start.location.y);

        do {
            currentTile = this.queue.shift();

            if (currentTile === this.end) {
                break;
            }

            for (let tile of this.getEligibleAdjacentTiles(currentTile)) {
                tile.previous = currentTile;
                this.queue.push(tile);
                this.visitMap.markMap(tile.location.x, tile.location.y);
            }

        } while(this.queue.length);

        this.tracePath(currentTile);
    }

    tracePath(endTile: Tile) {
        let tile = endTile;
        this.path = [];

        while(tile.previous !== null) {
            this.path.push(tile);
            tile = tile.previous;
        }
        this.path.push(tile);
    }

    /**
     * Returns an array of eligible tiles adjacent to the passed tile
     * @param {Tile} tile
     * @return {Tile[]}
     */
    getEligibleAdjacentTiles(tile: Tile) {
        const adjacentTiles: Tile[] = this.map.getAdjacentTiles(tile);
        const eligible: Tile[] = [];

        for (let neighbor of adjacentTiles) {
            if (this.tileIsEligible(neighbor)) {
                eligible.push(neighbor);
            }
        }

        return eligible;
    }

    /**
     * Returns true if the Tile is eligible for the path
     * @param {Tile} tile
     * @return {boolean}
     */
    tileIsEligible(tile: Tile) {

        return tile === this.end ? true :
            (tile.type === TileType.FLOOR || tile.type === TileType.BLANK) && !tile.furniture &&
            !this.visitMap.isMarkedAt(tile.location.x, tile.location.y) && !this.map.isInteriorTile(tile);
    }

    reset() {
        this.resetTiles();
        this.queue = [];
        delete this.start;
        delete this.end;
        delete this.map;
        delete this.visitMap;
        delete this.path;
    }

    resetTiles() {
        for (let i = 0; i < this.map.dimension; i++) {
            for (let j = 0; j < this.map.dimension; j++) {
                this.map.tiles[i][j].previous = null;
            }
        }
    }
}
