export class VisitMap {
    map: boolean[][];

    /**
     * Creates a VisitMap to track visited tiles
     * @return {any[]}
     */
    constructor(dimension: number) {
        this.map = [];

        let row;

        for (let i = 0; i < dimension; i++) {
            row = [];
            for (let j = 0; j < dimension; j++) {
                row[j] = false;
            }
            this.map.push(row);
        }
    }

    /**
     * Sets the value at map[y][x] to true
     * @param {number} x
     * @param {number} y
     */
    markMap(x: number, y: number) {
        this.map[y][x] = true;
    }

    /**
     * Returns the value at map[y][x]
     * @param {number} x
     * @param {number} y
     * @return {boolean}
     */
    isMarkedAt(x: number, y: number) {
        return this.map[y][x];
    }
}
