import {TestData} from '../test-data';
import {Door} from '../Tile/Objects/Furniture/Door/door';
import {TileType} from '../Tile/tile-type';
import {Pathfinder} from './pathfinder';
import {VisitMap} from './visit-map';

describe('Pathfinder', () => {
    let component: Pathfinder;

    beforeEach(() => {
        component = new Pathfinder();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.queue).toEqual([]);
    });

    it('reset', () => {
        const map = TestData.createMap();

        component.start = map.tiles[0][0];
        component.end = map.tiles[4][4];
        component.map = map;
        component.visitMap = new VisitMap(map.dimension);
        component.breadthFirstSearch();

        component.reset();
        expect(component).toEqual(new Pathfinder());
    });

    it('tileIsEligible', () => {
        const map = TestData.createMap();
        const interiorSpy = spyOn(map, 'isInteriorTile').and.returnValues(false, false, true);

        component.end = map.tiles[0][0];
        expect(component.tileIsEligible(map.tiles[0][0])).toBe(true);

        component.end = null;
        component.map = map;
        component.visitMap = new VisitMap(map.dimension);
        const testTile = map.tiles[0][0];
        
        testTile.type = TileType.FLOOR;
        expect(component.tileIsEligible(testTile)).toBe(true);

        testTile.type = TileType.BLANK;
        expect(component.tileIsEligible(testTile)).toBe(true);

        testTile.type = TileType.WALL;
        expect(component.tileIsEligible(testTile)).toBe(false);

        testTile.type = TileType.BLANK;
        testTile.furniture = new Door();
        expect(component.tileIsEligible(testTile)).toBe(false);

        testTile.furniture = null;
        component.visitMap.markMap(0,0);
        expect(component.tileIsEligible(testTile)).toBe(false);

        component.visitMap.map[0][0] = false;
        expect(component.tileIsEligible(testTile)).toBe(false);
        expect(interiorSpy).toHaveBeenCalledTimes(3);
    });

    it('resetTiles', () => {
        const map = TestData.createMap();
        component.map = map;

        for (let y = 0; y < map.dimension; y++) {
            for (let x = 0; x < map.dimension; x++) {
                map.tiles[y][x].previous = map.tiles[0][0];
            }
        }

        component.resetTiles();

        for (let y = 0; y < map.dimension; y++) {
            for (let x = 0; x < map.dimension; x++) {
                expect(map.tiles[0][0].previous).toEqual(null);
            }
        }
    });

    it('getEligibleAdjacentTiles', () => {
        const map = TestData.createMap();
        component.map = map;
        const eligibleSpy = spyOn(component, 'tileIsEligible').and.returnValues(true, true, false, true);

        expect(component.getEligibleAdjacentTiles(map.tiles[1][1]).length).toEqual(3);
        expect(eligibleSpy).toHaveBeenCalledTimes(4);
    });

    it('tracePath', () => {
        const map = TestData.createMap();
        component.map = map;

        map.tiles[0][1].previous = map.tiles[0][0];
        map.tiles[1][2].previous = map.tiles[0][1];

        component.tracePath(map.tiles[1][2]);

        expect(component.path.length).toEqual(3);
        expect(component.path).toEqual([
            map.tiles[1][2],
            map.tiles[0][1],
            map.tiles[0][0]
        ]);
    });

    it('findPath', () => {
        const map = TestData.createMap();
        component.map = map;
        const bfsSpy = spyOn(component, 'breadthFirstSearch').and.stub();

        component.findPath(map.tiles[0][0], map.tiles[5][5], map);

        expect(bfsSpy).toHaveBeenCalledTimes(1);
    });
});
