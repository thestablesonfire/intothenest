import {Map} from '../map';
import {TestData} from '../test-data';
import {VisitMap} from './visit-map';

describe('VisitMap', () => {
    let component: VisitMap;
    let map: Map;

    beforeEach(() => {
        map = TestData.createMap();
        component = new VisitMap(map.dimension);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.map.length).toBe(map.dimension);
        expect(component.map[0].length).toBe(map.dimension);
    });

    it('markMap', () => {
        component.markMap(0,0);
        expect(component.map[0][0]).toBe(true);
    });

    it('isMarkedAt', () => {
        component.map[0][0] = true;
        expect(component.isMarkedAt(0,0)).toBe(true);
        expect(component.isMarkedAt(0,1)).toBe(false);
    });
});
