import {Map} from './map';
import {Coordinate} from './Tile/Coordinate/coordinate';
import {Room} from './Room/room';
import {RoomFactory} from './Room/room-factory';
import {Tile} from './Tile/tile';
import {TileType} from './Tile/tile-type';

export class TestData {
    room: Room;
    map: Map;

    constructor() {
        this.room = TestData.createRoom();
        this.map = TestData.createMap();
    }

    static createRoom(location?: Coordinate) {
        const roomFactory = new RoomFactory();
        roomFactory.minDoors = 2;
        roomFactory.maxDoors = 2;
        roomFactory.minDimension = 7;
        roomFactory.maxDimension = 7;

        return roomFactory.createRoom(
            location ? location : new Coordinate(0, 0, 0)
        );
    }

    static createMap() {
        const map = new Map(0);
        let row;

        for (let y = 0; y < 40; y++) {
            row = [];
            for (let x = 0; x < 40; x++) {
                row.push(new Tile(
                    new Coordinate(x, y, map.zLevel),
                    TileType.BLANK
                ));
            }
            map.tiles.push(row);
        }

        return map;
    }
}