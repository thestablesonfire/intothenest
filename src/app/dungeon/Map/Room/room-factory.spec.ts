import {Coordinate} from '../Tile/Coordinate/coordinate';
import {Door} from '../Tile/Objects/Furniture/Door/door';
import {TileType} from '../Tile/tile-type';
import {Room} from './room';
import {RoomFactory} from './room-factory';

describe('RoomFactory', () => {
    let component: RoomFactory;

    beforeEach(() => {
        component = new RoomFactory();
        component.room = new Room(new Coordinate(0,0,0));
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.minDimension).toEqual(5);
        expect(component.maxDimension).toEqual(15);
        expect(component.minDoors).toEqual(1);
        expect(component.maxDoors).toEqual(4);
    });

    it('createRoom', () => {
        this.room = null;
        const dimensionSpy = spyOn(component, 'setDimensions');
        const tileSpy = spyOn(component, 'buildTiles');
        const indexSpy = spyOn(component, 'indexTiles');

        component.createRoom(new Coordinate(0,0,0));

        expect(component.room).toBeTruthy();
        expect(dimensionSpy).toHaveBeenCalledTimes(1);
        expect(tileSpy).toHaveBeenCalledTimes(1);
        expect(indexSpy).toHaveBeenCalledTimes(1);
    });

    it('setDimensions', () => {
        component.setDimensions();

        expect(component.room.width).toBeGreaterThanOrEqual(5);
        expect(component.room.height).toBeGreaterThanOrEqual(5);
        expect(component.room.width).toBeLessThanOrEqual(15);
        expect(component.room.height).toBeLessThanOrEqual(15);
    });

    it('buildTiles', () => {
        const generateSpy = spyOn(component, 'generateBlankRoom');
        const wallSpy = spyOn(component, 'setWalls');
        const doorSpy = spyOn(component, 'setDoors');

        component.buildTiles();

        expect(generateSpy).toHaveBeenCalledTimes(1);
        expect(wallSpy).toHaveBeenCalledTimes(1);
        expect(doorSpy).toHaveBeenCalledTimes(1);
    });

    it('generateBlankRoom', () => {
        component.room.height = 5;
        component.room.width = 6;
        component.generateBlankRoom();
        expect(component.room.tiles.length).toBe(5);
        expect(component.room.tiles[0].length).toBe(6);
    });

    it('setWalls', () => {
        component.room.height = 5;
        component.room.width = 5;
        component.generateBlankRoom();
        component.setWalls();

        for (let y = 1; y < 4; y++) {
            expect(component.room.tiles[y][1].type).toEqual(TileType.WALL);
            expect(component.room.tiles[y][3].type).toEqual(TileType.WALL);
        }
        for (let x = 1; x < 4; x++) {
            expect(component.room.tiles[1][x].type).toEqual(TileType.WALL);
            expect(component.room.tiles[3][x].type).toEqual(TileType.WALL);
        }
    });

    it('setWalls', () => {
        component.room.height = 5;
        component.room.width = 5;
        component.generateBlankRoom();
        component.setWalls();

        for (let y = 1; y < 4; y++) {
            expect(component.room.tiles[y][1].type).toEqual(TileType.WALL);
            expect(component.room.tiles[y][3].type).toEqual(TileType.WALL);
        }
        for (let x = 1; x < 4; x++) {
            expect(component.room.tiles[1][x].type).toEqual(TileType.WALL);
            expect(component.room.tiles[3][x].type).toEqual(TileType.WALL);
        }
    });

    it('setDoors', () => {
        component.minDoors = 2;
        component.maxDoors = 2;
        component.room.height = 5;
        component.room.width = 5;
        component.generateBlankRoom();
        component.setWalls();

        component.setDoors();

        expect(component.room.doors.length).toBe(2);
    });

    it('setDoors', () => {
        component.minDoors = 2;
        component.maxDoors = 2;
        component.room.height = 5;
        component.room.width = 5;
        component.generateBlankRoom();
        component.setWalls();

        component.setDoor();

        expect(component.room.doors.length).toBe(1);
        component.room = new Room(new Coordinate(0, 0, 0));
        component.room.height = 3;
        component.room.width = 5;
        component.generateBlankRoom();
        component.setWalls();

        component.setDoor();
        expect(component.room.doors.length).toBe(1);
        component.setDoor();
        expect(component.room.doors.length).toBe(1);
    });

    it('isDoorAdjacent', () => {
        component.minDoors = 1;
        component.maxDoors = 1;
        component.room.height = 5;
        component.room.width = 5;
        component.generateBlankRoom();

        component.room.tiles[2][2].furniture = new Door();
        const tiles = component.room.tiles;

        expect(component.isDoorAdjacent(tiles[1][1])).toBe(false);
        expect(component.isDoorAdjacent(tiles[1][2])).toBe(true);
        expect(component.isDoorAdjacent(tiles[1][3])).toBe(false);
        expect(component.isDoorAdjacent(tiles[2][1])).toBe(true);
        expect(component.isDoorAdjacent(tiles[2][3])).toBe(true);
        expect(component.isDoorAdjacent(tiles[3][1])).toBe(false);
        expect(component.isDoorAdjacent(tiles[3][2])).toBe(true);
        expect(component.isDoorAdjacent(tiles[3][3])).toBe(false);
        expect(component.isDoorAdjacent(tiles[0][0])).toBe(false);
        expect(component.isDoorAdjacent(tiles[4][4])).toBe(false);
    });

    it('indexTiles', () => {
        const perimeterSpy = spyOn(component, 'setPerimeterTiles');
        const interiorSpy = spyOn(component, 'setInteriorTiles');

        component.indexTiles();

        expect(perimeterSpy).toHaveBeenCalledTimes(1);
        expect(interiorSpy).toHaveBeenCalledTimes(1);
    });

    it('setInteriorTiles', () => {
        component.minDimension = 7;
        component.maxDimension = 7;
        component.room.height = 7;
        component.room.width = 7;
        component.generateBlankRoom();

        component.setInteriorTiles();

        expect(component.room.interior.length).toBe(9);
    });

    it('setPerimeterTiles', () => {
        component.minDimension = 5;
        component.maxDimension = 5;
        component.room.height = 5;
        component.room.width = 5;
        component.generateBlankRoom();

        component.setPerimeterTiles();

        expect(component.room.perimeter.length).toBe(16);
    });
});
