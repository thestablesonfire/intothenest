import {Coordinate} from '../Tile/Coordinate/coordinate';
import {Tile} from '../Tile/tile';
import {TileType} from '../Tile/tile-type';
import {Room} from './room';

describe('Room', () => {
    let component: Room;

    beforeEach(() => {
        component = new Room(new Coordinate(1, 1, 0));
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.location).toEqual(new Coordinate(1, 1, 0));
        expect(component.tiles).toEqual([]);
        expect(component.perimeter).toEqual([]);
        expect(component.interior).toEqual([]);
        expect(component.walls).toEqual([]);
        expect(component.doors).toEqual([]);
    });

    it('isInteriorTile', () => {
        let tile = new Tile(new Coordinate(1, 1, 0), TileType.FLOOR);
        component.tiles = [[tile]];
        component.interior.push(tile);

        expect(component.isInteriorTile(tile)).toBe(true);

        tile = new Tile(new Coordinate(0, 0, 0), TileType.FLOOR);
        expect(component.isInteriorTile(tile)).toBe(false);
    });
});
