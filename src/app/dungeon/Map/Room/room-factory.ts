import {Coordinate} from '../Tile/Coordinate/coordinate';
import {Door} from '../Tile/Objects/Furniture/Door/door';
import {Tile} from '../Tile/tile';
import {TileType} from '../Tile/tile-type';
import {UtilitiesService} from '../Utilities/utilities.service';
import {Room} from './room';

export class RoomFactory {
    /**
     * The Room currently being built
     */
    room: Room;

    /**
     * The minimum dimension a room may have
     */
    minDimension: number;

    /**
     * The maximum dimension a room may have
     */
    maxDimension: number;

    /**
     * The minimum number of doors per room
     */
    minDoors: number;

    /**
     * The maximum number of doors per room
     */
    maxDoors: number;

    /**
     * A single Room
     */
    constructor() {
        this.minDimension = 5;
        this.maxDimension = 15;
        this.minDoors = 1;
        this.maxDoors = 4;
    }

    /**
     * Creates a new Room
     * @param {Coordinate} location
     * @return {Room}
     */
    createRoom(location: Coordinate) {
        this.room = new Room(location);
        this.setDimensions();
        this.buildTiles();
        this.indexTiles();

        return this.room;
    }

    /**
     * Randomly sets the dimensions of the new room
     */
    setDimensions() {
        this.room.width = UtilitiesService.generateRandomNumber(this.minDimension, this.maxDimension);
        this.room.height = UtilitiesService.generateRandomNumber(this.minDimension, this.maxDimension);
    }

    /**
     * Builds all the tiles for the room
     */
    buildTiles() {
        this.generateBlankRoom();
        this.setWalls();
        this.setDoors();
    }

    /**
     * Sets all the tiles to blank FLOOR tiles
     */
    generateBlankRoom() {
        let row;

        for (let i =  0; i < this.room.height; i++) {

            row = [];
            for (let j = 0; j < this.room.width; j++) {
                row.push(new Tile(
                    new Coordinate(
                        this.room.location.x + j,
                        this.room.location.y + i,
                        this.room.location.z
                    ),
                    TileType.FLOOR
                ));
            }
            this.room.tiles.push(row);
        }
    }

    /**
     * Sets the walls of the room
     */
    setWalls() {
        const wallMaxHeight = this.room.height - 2;
        const wallMaxWidth = this.room.width - 2;

        this.setHorizontalWall(1, wallMaxWidth, 1);
        this.setHorizontalWall(1, wallMaxWidth, wallMaxHeight);
        this.setVerticalWall(2, wallMaxHeight - 1, wallMaxWidth);
        this.setVerticalWall(2, wallMaxHeight - 1, 1);
    }

    /**
     * Creates a horizontal wall in the room tiles
     * @param xStart
     * @param xEnd
     * @param y
     */
    setHorizontalWall(xStart, xEnd, y) {
        for (let x = xStart; x <= xEnd; x++) {
            this.room.tiles[y][x].type = TileType.WALL;
            this.room.walls.push(this.room.tiles[y][x]);
        }
    }

    /**
     * Creates a vertical wall in the room tiles
     * @param yStart
     * @param yEnd
     * @param x
     */
    setVerticalWall(yStart, yEnd, x) {
        for (let y = yStart; y <= yEnd; y++) {
            this.room.tiles[y][x].type = TileType.WALL;
            this.room.walls.push(this.room.tiles[y][x]);
        }
    }

    /**
     * Picks a random number of doors to set in the room and sets them
     */
    setDoors() {
        const numDoors = UtilitiesService.generateRandomNumber(this.minDoors, this.maxDoors);

        for (let i = 0; i < numDoors; i++) {
            this.setDoor();
        }
    }

    /**
     * Puts a door in one of the non-corner wall tiles
     */
    setDoor() {
        let randomDoorTile = this.getRandomWallTile();
        let attemptCount = 0;

        while((this.isCornerTile(randomDoorTile) ||
                randomDoorTile.hasDoor() ||
                this.isDoorAdjacent(randomDoorTile)) && attemptCount <= 500) {
            randomDoorTile = this.getRandomWallTile();
            attemptCount++;
        }

        if (attemptCount <= 500) {
            randomDoorTile.type = TileType.FLOOR;
            randomDoorTile.setFurniture(new Door());
            this.room.doors.push(randomDoorTile);
        }
    }

    /**
     * Returns true if the passed tile is adjacent to a Door
     * @param {Tile} tile
     * @return {boolean}
     */
    isDoorAdjacent(tile: Tile) {
        let doorAdjacent: boolean = false;
        
        const localLocation = tile.location.subtract2D(this.room.location);

        if (localLocation.x - 1 >= 0) {
            doorAdjacent = this.room.tiles[localLocation.y][localLocation.x - 1].hasDoor();
        }

        if (localLocation.x + 1 <= this.room.width - 1) {
            doorAdjacent = this.room.tiles[localLocation.y][localLocation.x + 1].hasDoor() || doorAdjacent;
        }

        if (localLocation.y - 1 >= 0) {
            doorAdjacent = this.room.tiles[localLocation.y - 1][localLocation.x].hasDoor() || doorAdjacent;
        }

        if (localLocation.y + 1 <= this.room.height - 1) {
            doorAdjacent = this.room.tiles[localLocation.y + 1][localLocation.x].hasDoor() || doorAdjacent;
        }

        return doorAdjacent;
    }

    /**
     * Returns true if the passed tile is in the corner of the wall
     * @param {Tile} tile
     * @return {boolean}
     */
    isCornerTile(tile: Tile) {
        const location = tile.location.subtract2D(this.room.location);
        const maxX = this.room.width - 2;
        const maxY = this.room.height - 2;

        return location.x === 1 && location.y === 1
            || location.x === 1 && location.y === maxY
            || location.x === maxX && location.y === 1
            || location.x === maxX && location.y === maxY;
    }

    /**
     * Returns a random wall Tile
     * @return {Tile}
     */
    getRandomWallTile() {
        return this.room.walls[UtilitiesService.generateRandomNumber(0, this.room.walls.length - 1)];
    }

    /**
     * Sets the perimeter and interior tile arrays for the room for quick referencing and searching
     */
    indexTiles() {
        this.setPerimeterTiles();
        this.setInteriorTiles();
    }

    /**
     * Sets all interior tiles of a room
     */
    setInteriorTiles() {
        for (let i = 2; i < this.room.height - 2; i++) {
            for (let j = 2; j < this.room.width - 2; j++) {
                this.room.interior.push(this.room.tiles[i][j]);
            }
        }
    }

    /**
     * Sets all perimeter tiles of a room
     */
    setPerimeterTiles() {
        let i;

        for (i = 0; i < this.room.width; i++) {
            this.room.perimeter.push(this.room.tiles[0][i]);
            this.room.perimeter.push(this.room.tiles[this.room.height - 1][i]);
        }

        for (i = 1; i < this.room.height - 1; i++) {
            this.room.perimeter.push(this.room.tiles[i][0]);
            this.room.perimeter.push(this.room.tiles[i][this.room.width - 1]);
        }
    }
}
