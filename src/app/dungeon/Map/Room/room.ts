import {Coordinate} from '../Tile/Coordinate/coordinate';
import {Tile} from '../Tile/tile';

export class Room {
    // TODO: Add name generation
    name: string;

    location: Coordinate;
    height: number;
    width: number;
    tiles: Tile[][];
    perimeter: Tile[];
    interior: Tile[];
    walls: Tile[];
    doors: Tile[];

    /**
     * A single Room
     * @param {Coordinate} location
     */
    constructor(location: Coordinate) {
        this.location = location;
        this.tiles = [];
        this.perimeter = [];
        this.interior = [];
        this.walls = [];
        this.doors = [];
    }

    /**
     * Returns true if the passed tile is part of the interior of the room
     * @param {Tile} tile
     * @return {any}
     */
    isInteriorTile(tile: Tile) {
        return this.interior.includes(tile);
    }

    // /**
    //  * Debug method that prints the room to console
    //  */
    // printRoom() {
    //     for (let y = 0; y < this.height; y++) {
    //
    //         let row = '';
    //         for (let x = 0; x < this.width; x++) {
    //             row += this.tiles[y][x].getIcon() + ' ';
    //         }
    //         console.log(row);
    //     }
    // }
}
