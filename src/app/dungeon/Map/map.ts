import {MapFactory} from './map-factory';
import {Room} from './Room/room';
import {RoomFactory} from './Room/room-factory';
import {Coordinate} from './Tile/Coordinate/coordinate';
import {Tile} from './Tile/tile';

export class Map {
    /**
     * The width and height of each map
     */
    dimension: number;

    /**
     * The tiles for the map
     */
    tiles: Tile[][];

    /**
     * The rooms in the map
     */
    rooms: Room[];

    /**
     * The depth of the current Map
     */
    zLevel: number;

    /**
     * A single Map
     * @param {number} zLevel
     */
    constructor(zLevel: number) {
        this.dimension = 40;
        this.tiles = [];
        this.rooms = [];
        this.zLevel = zLevel;
    }

    /**
     * Returns the tiles adjacent to the passed tile
     * @param {Tile} tile
     * @param {boolean} corner - If true, also returns the adjacent corner tiles
     * @return {Tile[]}
     */
    getAdjacentTiles(tile: Tile, corner?: boolean) {
        const tiles: Tile[] = [];
        const location: Coordinate = tile.location;
        const xLocation: number = location.x;
        const yLocation: number = location.y;

        if (yLocation + 1 < this.dimension) {
            tiles.push(this.tiles[yLocation + 1][xLocation]);
        }

        if (yLocation - 1 >= 0) {
            tiles.push(this.tiles[yLocation - 1][xLocation]);
        }

        if (xLocation + 1 < this.dimension) {
            tiles.push(this.tiles[yLocation][xLocation + 1]);
        }

        if (xLocation - 1 >= 0) {
            tiles.push(this.tiles[yLocation][xLocation - 1]);
        }

        if (corner) {
            if (yLocation - 1 >= 0 && xLocation - 1 >= 0) {
                tiles.push(this.tiles[yLocation - 1][xLocation - 1]);
            }

            if (yLocation - 1 >= 0 && xLocation + 1 < this.dimension) {
                tiles.push(this.tiles[yLocation - 1][xLocation + 1]);
            }

            if (yLocation + 1 < this.dimension && xLocation + 1 < this.dimension) {
                tiles.push(this.tiles[yLocation + 1][xLocation + 1]);
            }

            if (yLocation + 1 < this.dimension && xLocation - 1 >= 0) {
                tiles.push(this.tiles[yLocation + 1][xLocation - 1]);
            }
        }

        return tiles;
    }

    /**
     * Returns true if the passed tile is part of the interior of any room
     * @param {Tile} tile
     * @return {boolean}
     */
    isInteriorTile(tile: Tile) {
        let interior = false;

        for (let room of this.rooms) {
            if (room.isInteriorTile(tile)) {
                interior = true;
            }
        }

        return interior;
    }

    // /**
    //  * DEBUG method that prints the current map
    //  */
    // printMap() {
    //     for (let y = 0; y < this.dimension; y++) {
    //
    //         let row = '';
    //         for (let x = 0; x < this.dimension; x++) {
    //             row += this.tiles[y][x].getIcon() + ' ';
    //         }
    //         console.log(row);
    //     }
    // }
}
