import {Map} from './map';
import {MapFactory} from './map-factory';
import {TestData} from './test-data';
import {Coordinate} from './Tile/Coordinate/coordinate';
import {TileType} from './Tile/tile-type';

describe('MapFactory', () => {
    let component: MapFactory;
    let testData: TestData;

    beforeEach(() => {
        component = new MapFactory();
        component.map = new Map(0);
        testData = new TestData();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.roomFactory).toBeTruthy();
        expect(component.minRooms).toEqual(6);
        expect(component.maxRooms).toEqual(9);
        expect(component.dimension).toEqual(40);
    });

    it('createMap', () => {
        const map = component.createMap(0);

        expect(map.rooms.length).toBeGreaterThanOrEqual(6);
        expect(map.rooms.length).toBeLessThanOrEqual(9);
        expect(map.dimension).toEqual(40);
    });

    it('generateBlankMap', () => {
        component.generateBlankMap();

        expect(component.map.tiles.length).toEqual(40);
        expect(component.map.tiles[0].length).toEqual(40);

        for (let y = 0; y < 40; y++) {
            for (let x = 0; x < 40; x++) {
                expect(component.map.tiles[y][x].type).toEqual(TileType.BLANK);
            }
        }
    });

    it('roomFitsOnMap', () => {
        let room = testData.room;
        expect(component.roomFitsOnMap(room)).toBe(true);

        room.location = new Coordinate(39, 0, 0);
        expect(component.roomFitsOnMap(room)).toBe(false);

        room.location = new Coordinate(0, 39, 0);
        expect(component.roomFitsOnMap(room)).toBe(false);
    });

    it('addRoom', () => {
        expect(component.map.rooms.length).toEqual(0);
        const room = testData.room;
        const addTilesSpy = spyOn(component, 'addTiles');
        component.addRoom(room);
        expect(component.map.rooms.length).toEqual(1);
        expect(addTilesSpy).toHaveBeenCalledTimes(1);
    });

    it('addTiles', () => {
        component.map = testData.map;
        expect(component.map.tiles[0][0].type).toEqual(TileType.BLANK);
        component.addRoom(testData.room);
        expect(component.map.tiles[0][0].type).toEqual(TileType.FLOOR);
        expect(component.map.tiles[1][1].type).toEqual(TileType.WALL || TileType.FLOOR);
        expect(component.map.tiles[2][2].type).toEqual(TileType.FLOOR);
    });

    it('roomDoesNotOverlap', () => {
        const room1 = testData.room;
        const room2 = TestData.createRoom();
        component.map = testData.map;
        component.addRoom(room1);

        expect(component.roomDoesNotOverlap(room2)).toEqual(false);
        room2.location = new Coordinate(20, 20, 0);
        expect(component.roomDoesNotOverlap(room2)).toEqual(true);
    });

    it('fillBlankParameters', () => {
        component.map = TestData.createMap();
        const room = TestData.createRoom(new Coordinate(1, 1, 0));

        component.addRoom(room);
        component.fillBlankPerimeters();

        for (let x = 0; x < 9; x++) {
            expect(component.map.tiles[0][x].type).toEqual(TileType.WALL);
            expect(component.map.tiles[8][x].type).toEqual(TileType.WALL);
        }

        for (let y = 1; y < 8; y++) {
            expect(component.map.tiles[y][0].type).toEqual(TileType.WALL);
            expect(component.map.tiles[y][8].type).toEqual(TileType.WALL);
        }
    });

    it('createHallway', () => {
        component.map = TestData.createMap();
        const path = [];
        const tiles = component.map.tiles;
        path.push(tiles[1][1]);
        path.push(tiles[1][2]);
        path.push(tiles[2][2]);
        path.push(tiles[2][3]);

        component.createHallway(path);

        expect(tiles[1][1].type).toEqual(TileType.FLOOR);
        expect(tiles[1][2].type).toEqual(TileType.FLOOR);
        expect(tiles[2][2].type).toEqual(TileType.FLOOR);
        expect(tiles[2][3].type).toEqual(TileType.FLOOR);

        expect(tiles[0][0].type).toEqual(TileType.WALL);
        expect(tiles[0][1].type).toEqual(TileType.WALL);
        expect(tiles[0][2].type).toEqual(TileType.WALL);
        expect(tiles[0][3].type).toEqual(TileType.WALL);
        expect(tiles[1][0].type).toEqual(TileType.WALL);
        expect(tiles[2][0].type).toEqual(TileType.WALL);
        expect(tiles[1][3].type).toEqual(TileType.WALL);
        expect(tiles[1][4].type).toEqual(TileType.WALL);
        expect(tiles[2][1].type).toEqual(TileType.WALL);
        expect(tiles[2][4].type).toEqual(TileType.WALL);
        expect(tiles[3][1].type).toEqual(TileType.WALL);
        expect(tiles[3][2].type).toEqual(TileType.WALL);
        expect(tiles[3][3].type).toEqual(TileType.WALL);
        expect(tiles[3][4].type).toEqual(TileType.WALL);
    });

    it('createHallways', () => {
        component.map = TestData.createMap();
        const room1 = TestData.createRoom(new Coordinate(5, 5, 0));
        const room2 = TestData.createRoom(new Coordinate(15, 15, 0));
        const hallwaySpy = spyOn(component, 'createHallway');

        component.addRoom(room1);
        component.addRoom(room2);

        component.createHallways();

        expect(hallwaySpy).toHaveBeenCalledTimes(4);
    });

    it('getOtherRooms', () => {
        component.map = TestData.createMap();
        const room1 = TestData.createRoom(new Coordinate(5, 5, 0));
        const room2 = TestData.createRoom(new Coordinate(15, 15, 0));
        const room3 = TestData.createRoom(new Coordinate(0, 15, 0));

        component.addRoom(room1);
        component.addRoom(room2);
        component.addRoom(room3);

        expect(component.getOtherRooms(room1)).toEqual([room2, room3]);
        expect(component.getOtherRooms(room2)).toEqual([room1, room3]);
    });

    it('getAllDoors', () => {
        component.map = TestData.createMap();
        const room1 = TestData.createRoom(new Coordinate(5, 5, 0));
        const room2 = TestData.createRoom(new Coordinate(15, 15, 0));
        const room3 = TestData.createRoom(new Coordinate(0, 15, 0));

        const doors = component.getAllDoors([room1, room2, room3]);

        expect(doors.length).toBe(6);
    });

    it('createRooms', () => {
        component.map = TestData.createMap();
        component.createRooms();
        expect(component.map.rooms.length).toBeGreaterThanOrEqual(6);
        expect(component.map.rooms.length).toBeLessThanOrEqual(9);
    });
});
