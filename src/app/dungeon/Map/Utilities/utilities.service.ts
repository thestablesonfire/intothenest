import { Injectable } from '@angular/core';
import {Map} from '../map';
import {Pathfinder} from '../Pathfinder/pathfinder';
import {Coordinate} from '../Tile/Coordinate/coordinate';
import {Tile} from '../Tile/tile';

@Injectable()
export class UtilitiesService {
    pathFinder: Pathfinder;

    /**
     * Static Class with generic utilities used throughout
     */
    constructor() {
        this.pathFinder = new Pathfinder();
    }

    /**
     * Returns a random integer between min and max
     * @param min
     * @param max
     * @return {any}
     */
    static generateRandomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /**
     * Generates a random Coordinate given the max X and Y dimensions and sets the z to z
     * @param {number} xDimension
     * @param {number} yDimension
     * @param {number} z
     * @return {Coordinate}
     */
    static generateRandomCoordinate(xDimension: number, yDimension: number, z: number) {
        return new Coordinate(
            this.generateRandomNumber(0, xDimension - 1),
            this.generateRandomNumber(0, yDimension - 1),
            z
        );
    }

    /**
     * Finds a path between two tiles
     * @param {Tile} tile1
     * @param {Tile} tile2
     * @param {Map} map
     */
    findPath(tile1: Tile, tile2: Tile, map: Map) {
        return  this.pathFinder.findPath(tile1, tile2, map);
    }
}
