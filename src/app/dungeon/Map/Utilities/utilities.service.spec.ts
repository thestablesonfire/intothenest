import { TestBed, inject } from '@angular/core/testing';
import {TestData} from '../test-data';

import { UtilitiesService } from './utilities.service';

describe('UtilitiesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UtilitiesService]
    });
  });

  it('should be created', inject([UtilitiesService], (service: UtilitiesService) => {
    expect(service).toBeTruthy();
  }));

    it('generateRandomNumber', () => {
        let number = UtilitiesService.generateRandomNumber(5, 5);

        expect(number).toEqual(5);

        number = UtilitiesService.generateRandomNumber(5, 6);
        expect(number).toBeGreaterThanOrEqual(5);
        expect(number).toBeLessThanOrEqual(6);
    });

    it('generateRandomCoordinate', () => {
        let coordinate = UtilitiesService.generateRandomCoordinate(5, 5, 0);

        expect(coordinate.x).toBeLessThan(5);
        expect(coordinate.y).toBeLessThan(5);
        expect(coordinate.x).toBeGreaterThanOrEqual(0);
        expect(coordinate.y).toBeGreaterThanOrEqual(0);
        expect(coordinate.z).toBe(0);
    });

    it('should be created', inject([UtilitiesService], (service: UtilitiesService) => {
        const map = TestData.createMap();
        const findPathSpy = spyOn(service.pathFinder, 'findPath');

        service.findPath(map.tiles[0][0], map.tiles[1][1], map);

        expect(findPathSpy).toHaveBeenCalledTimes(1);
    }));
});
