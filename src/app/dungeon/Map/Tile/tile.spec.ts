
import {Door} from './Objects/Furniture/Door/door';
import {FurnitureType} from './Objects/Furniture/furniture-type';
import {Item} from './Objects/Item/item';
import {Tile} from './tile';
import {Coordinate} from './Coordinate/coordinate';
import {TileType} from './tile-type';
import createSpyObj = jasmine.createSpyObj;

describe('Tile', () => {
    let component: Tile;

    beforeEach(() => {
        component = new Tile(
            new Coordinate(0, 0, 0),
            TileType.BLANK
        );
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.location).toEqual(new Coordinate(0,0,0));
        expect(component.type).toEqual(TileType.BLANK);
        expect(component.furniture).toEqual(null);
        expect(component.items).toEqual([]);
        expect(component.previous).toEqual(null);
    });

    it('hasDoor', () => {
        expect(component.hasDoor()).toBe(false);
        component.furniture = new Door();
        expect(component.hasDoor()).toBe(true);
        component.furniture.type = FurnitureType.DOOR_OPEN;
        expect(component.hasDoor()).toBe(true);
    });

    it('getIcon', () => {
        const doorSpy = createSpyObj('Door', {'getIcon': FurnitureType.DOOR_CLOSED.icon});
        expect(component.getIcon()).toBe(TileType.BLANK.icon);

        component.furniture = doorSpy;
        expect(component.getIcon()).toEqual(FurnitureType.DOOR_CLOSED.icon);
    });

    it('getColor', () => {
        const doorSpy = createSpyObj('Door', {'getColor': FurnitureType.DOOR_CLOSED.color});
        expect(component.getColor()).toBe(TileType.BLANK.color);

        component.furniture = doorSpy;
        expect(component.getColor()).toEqual(FurnitureType.DOOR_CLOSED.color);
    });

    it('getFurniture', () => {
        expect(component.getFurniture()).toEqual(null);
        component.furniture = new Door();
        expect(component.getFurniture()).toEqual(new Door());
    });

    it('setFurniture', () => {
        const door = new Door();
        component.setFurniture(door);

        expect(component.furniture).toBe(door);
        expect(component.furniture.location).toEqual(component.location);

        component.location = new Coordinate(1,1,0);
        component.setFurniture(new Door());

        expect(component.furniture.location).toEqual(new Coordinate(0,0,0));
    });

    it('removeFurniture', () => {
        component.furniture = new Door();
        component.removeFurniture();
        expect(component.furniture).toEqual(null);
    });

    it('addItem', () => {
        const item = new Item();

        component.addItem(item);
        expect(component.items.length).toEqual(1);

        component.addItem(item);
        component.addItem(item);
        component.addItem(item);
        component.addItem(item);
        expect(component.items.length).toEqual(5);
    });
});
