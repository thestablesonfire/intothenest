import {Icon} from './icon';

/**
 * Holds the types of Tiles
 */
export class TileType {
    static BLANK = new Icon('.', '#FFF');
    static FLOOR = new Icon('_', '#d3d3d3');
    static WALL = new Icon('█', '#a9a9a9');
}
