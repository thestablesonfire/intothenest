export class Coordinate {
    /**
     * A coordinate that denotes a location on the map
     * @param {number} x
     * @param {number} y
     * @param {number} z
     */
    constructor(
        public x: number,
        public y: number,
        public z: number
    ) {}

    /**
     * Subtracts another Coordinate from this one, keeping this z value
     * @param {Coordinate} coordinate
     * @return {Coordinate}
     */
    subtract2D(coordinate: Coordinate) {
        return new Coordinate(this.x - coordinate.x, this.y - coordinate.y, this.z)
    }

    /**
     * Adds another coordinate to this one, keeping this z value
     * @param {Coordinate} coordinate
     * @return {Coordinate}
     */
    add2D(coordinate: Coordinate) {
        return new Coordinate(this.x + coordinate.x, this.y + coordinate.y, this.z);
    }

    /**
     * Returns true if the coordinates are the same
     * @param {Coordinate} coordinate
     * @return {boolean}
     */
    equals(coordinate: Coordinate) {
        return this.x === coordinate.x && this.y === coordinate.y && this.z === coordinate.z;
    }
}
