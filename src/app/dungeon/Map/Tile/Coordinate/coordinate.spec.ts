import {Coordinate} from './coordinate';

describe('Coordinate', () => {
    let component: Coordinate;

    beforeEach(() => {
        component = new Coordinate(0, 0, 0);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.x).toEqual(0);
        expect(component.y).toEqual(0);
        expect(component.z).toEqual(0);
    });

    it('subtract2D', () => {
        expect(component.subtract2D(new Coordinate(1,1,1))).toEqual(new Coordinate(-1,-1,0));
    });

    it('add2D', () => {
        expect(component.add2D(new Coordinate(1,1,1))).toEqual(new Coordinate(1,1,0));
    });

    it('equals', () => {
        expect(component.equals(new Coordinate(0,0,0))).toEqual(true);
        expect(component.equals(new Coordinate(1,0,0))).toEqual(false);
        expect(component.equals(new Coordinate(0,1,0))).toEqual(false);
        expect(component.equals(new Coordinate(0,0,1))).toEqual(false);
    });
});
