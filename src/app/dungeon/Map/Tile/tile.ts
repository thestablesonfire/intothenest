import {Icon} from './icon';
import {Coordinate} from './Coordinate/coordinate';
import {Furniture} from './Objects/Furniture/furniture';
import {FurnitureType} from './Objects/Furniture/furniture-type';
import {Item} from './Objects/Item/item';
import {TileType} from './tile-type';

export class Tile {
    items: Item[];
    furniture: Furniture;
    location: Coordinate;
    type: Icon;
    previous: Tile;

    /**
     * Returns true if this tile has a Door
     * @return {boolean | boolean}
     */
    hasDoor() {
        return this.furniture ?
            this.furniture.type === FurnitureType.DOOR_CLOSED || this.furniture.type === FurnitureType.DOOR_OPEN :
            false;
    }

    /**
     * A single location on the map and its contents and type
     * @param {Coordinate} location
     * @param {Icon} type
     */
    constructor(location: Coordinate, type: Icon) {
        this.location = location;
        this.type = type;
        this.furniture = null;
        this.items = [];
        this.previous = null;
    }

    /**
     * Returns the icon for the Tile
     * @return {string | TileType.type}
     */
    getIcon() {
        return this.furniture && this.furniture.getIcon() ? this.furniture.getIcon() : this.type.icon;
    }

    /**
     * Returns the color for the icon for the Tile
     * @return {string}
     */
    getColor() {
        return this.furniture && this.furniture.getColor() ? this.furniture.getColor() : this.type.color;
    }

    /**
     * Returns the furniture for this tile
     * @return {Furniture}
     */
    getFurniture() {
        return this.furniture;
    }

    /**
     * Adds a Furniture item to the tile
     * @param {Furniture} furniture
     */
    setFurniture(furniture: Furniture) {
        if (!this.furniture) {
            this.furniture = furniture;
            this.furniture.location = this.location;
        }
    }

    /**
     * Removes the furniture on a tile
     */
    removeFurniture() {
        this.furniture = null;
    }

    /**
     * Adds an item to the Tile's inventory
     * @param {Item} item
     */
    addItem(item: Item) {
        this.items.push(item);
    }
}
