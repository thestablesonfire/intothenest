import {Icon} from '../../icon';
import {Coordinate} from '../../Coordinate/coordinate';
import {FurnitureType} from './furniture-type';

export class Furniture {
    /**
     * The location the furniture is installed in
     */
    location: Coordinate;

    /**
     * If a creature can walk on the tile this furniture occupies
     */
    isPassable: boolean;

    /**
     * If a creature can interact with the furniture
     */
    canInteract: boolean;

    /**
     * What happens if you interact with the object
     */
    interactAction: any;

    /**
     * If object can be inspected
     */
    isInspectable: boolean;

    /**
     * The type of furniture this is
     */
    type: Icon;

    /**
     * A generic piece of furniture on the map
     */
    constructor() {
        this.type = FurnitureType.FURNITURE;
        this.isPassable = true;
        this.canInteract = true;
        this.isInspectable = true;
    }

    /**
     * Returns the icon for this piece of Furniture
     * @return {string}
     */
    getIcon() {
        return this.type.icon;
    }

    /**
     * Returns the color the the icon for this piece of Furniture
     * @return {string}
     */
    getColor() {
        return this.type.color;
    }

    /**
     * Interacting with the object
     */
    interact() {
        if (this.canInteract && this.interactAction) {
            this.interactAction();
        }
    }
}
