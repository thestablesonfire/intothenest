import {Furniture} from './furniture';
import {FurnitureType} from './furniture-type';

describe('Furniture', () => {
    let component: Furniture;

    beforeEach(() => {
        component = new Furniture();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.type).toEqual(FurnitureType.FURNITURE);
        expect(component.isPassable).toEqual(true);
        expect(component.canInteract).toEqual(true);
        expect(component.isInspectable).toEqual(true);
    });

    it('getIcon', () => {
        expect(component.getIcon()).toEqual(FurnitureType.FURNITURE.icon);
    });

    it('getColor', () => {
        expect(component.getColor()).toEqual(FurnitureType.FURNITURE.color);
    });

    it('interact', () => {
        component.interactAction = function () {
            this.isPassable = !this.isPassable;
        };

        component.interact();
        expect(component.isPassable).toEqual(false);

        component.canInteract = false;
        component.interact();
        expect(component.isPassable).toEqual(false);
    });
});
