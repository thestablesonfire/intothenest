import {FurnitureType} from '../furniture-type';
import {Door} from './door';

describe('Door', () => {
    let component: Door;

    beforeEach(() => {
        component = new Door();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.type).toEqual(FurnitureType.DOOR_CLOSED);
        expect(component.isPassable).toEqual(false);
        expect(component.canInteract).toEqual(true);
        expect(component.isInspectable).toEqual(true);
        expect(component.interactAction).toEqual(component.toggleDoorOpen);
    });

    it('toggleDoorOpen', () => {
        component.toggleDoorOpen();
        expect(component.isPassable).toEqual(true)
        expect(component.type).toEqual(FurnitureType.DOOR_OPEN)
    });

    it('getIcon', () => {
        expect(component.getIcon()).toEqual(FurnitureType.DOOR_CLOSED.icon);
        component.interact();
        expect(component.getIcon()).toEqual(FurnitureType.DOOR_OPEN.icon);
    });

    it('getType', () => {
        expect(component.getType()).toEqual(FurnitureType.DOOR_CLOSED);
        component.interact();
        expect(component.getType()).toEqual(FurnitureType.DOOR_OPEN);
    });
});
