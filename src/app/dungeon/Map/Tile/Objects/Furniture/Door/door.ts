import {Furniture} from '../furniture';
import {FurnitureType} from '../furniture-type';

export class Door extends Furniture {
    /**
     * A door that opens and closes
     */
    constructor() {
        super();
        this.canInteract = true;
        this.isPassable = false;
        this.isInspectable = true;
        this.type = FurnitureType.DOOR_CLOSED;
        this.interactAction = this.toggleDoorOpen;
    }

    /**
     * Toggles whether the door is open or not
     */
    toggleDoorOpen() {
        this.isPassable = !this.isPassable;
        this.type = this.getType();
    }

    /**
     * Returns the icon for the door
     * @return {FurnitureType.DOOR_OPEN | FurnitureType.DOOR_CLOSED}
     */
    getIcon() {
        return this.isPassable ? FurnitureType.DOOR_OPEN.icon : FurnitureType.DOOR_CLOSED.icon;
    }

    /**
     * Returns the type based on whether the door is open or not
     * @return {Icon}
     */
    getType() {
        return this.isPassable ? FurnitureType.DOOR_OPEN : FurnitureType.DOOR_CLOSED;
    }
}
