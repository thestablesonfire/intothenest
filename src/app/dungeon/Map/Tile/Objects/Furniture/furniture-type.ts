import {Icon} from '../../icon';

/**
 * Holds the types of Furniture
 */
export class FurnitureType {
    static FURNITURE = new Icon('F', '#FFF');
    static DOOR_OPEN = new Icon(' ', '#FFF');
    static DOOR_CLOSED = new Icon('D', '#af7324');
}
