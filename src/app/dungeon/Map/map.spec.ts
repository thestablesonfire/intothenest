import {Map} from './map';
import {Room} from './Room/room';
import {Coordinate} from './Tile/Coordinate/coordinate';
import {Tile} from './Tile/tile';
import {TileType} from './Tile/tile-type';
import createSpyObj = jasmine.createSpyObj;

describe('Map', () => {
    let component: Map;

    const tiles = [];
    let row;

    for (let y = 0; y < 40; y++) {
        row = [];
        for (let x = 0; x < 40; x++) {
            row.push(new Tile(new Coordinate(x, y, 0), TileType.BLANK));
        }
        tiles.push(row);
    }

    beforeEach(() => {
        component = new Map(0);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.dimension).toEqual(40);
        expect(component.tiles).toEqual([]);
        expect(component.rooms).toEqual([]);
        expect(component.zLevel).toEqual(0);
    });

    it('getAdjacentTiles', () => {
        component.tiles = tiles;
        let adj = component.getAdjacentTiles(tiles[15][15], true);

        expect(adj.length).toEqual(8);
        expect(adj[0].location).toEqual(new Coordinate(15, 16, 0));
        expect(adj[1].location).toEqual(new Coordinate(15, 14, 0));
        expect(adj[2].location).toEqual(new Coordinate(16, 15, 0));
        expect(adj[3].location).toEqual(new Coordinate(14, 15, 0));
        expect(adj[4].location).toEqual(new Coordinate(14, 14, 0));
        expect(adj[5].location).toEqual(new Coordinate(16, 14, 0));
        expect(adj[6].location).toEqual(new Coordinate(16, 16, 0));
        expect(adj[7].location).toEqual(new Coordinate(14, 16, 0));

        adj = component.getAdjacentTiles(tiles[15][15], false);
        expect(adj.length).toEqual(4);

        adj = component.getAdjacentTiles(tiles[0][0], true);
        expect(adj.length).toEqual(3);

        adj = component.getAdjacentTiles(tiles[39][39], true);
        expect(adj.length).toEqual(3);
    });

    it('isInteriorTile', () => {
        const yesRoom = createSpyObj('Room', {'isInteriorTile': true});
        const noRoom = createSpyObj('Room', {'isInteriorTile': false});
        component.tiles = tiles;
        component.rooms.push(yesRoom, noRoom);

        expect(component.isInteriorTile(component.tiles[0][0])).toEqual(true);

        component.rooms.shift();
        expect(component.isInteriorTile(component.tiles[0][0])).toEqual(false);
    });
});
