import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {DungeonComponent} from './dungeon/dungeon.component';
import {MapFactory} from './dungeon/Map/map-factory';
import {MessageBoxComponent} from './message-box/message-box.component';
import {CommandInputComponent} from './command-input/command-input.component';


@NgModule({
  declarations: [
    AppComponent,
    DungeonComponent,
    MessageBoxComponent,
    CommandInputComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
      MapFactory
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
